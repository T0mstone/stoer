use std::borrow::Cow;
use std::ffi::OsString;

use snafu::{OptionExt, ResultExt, Whatever, whatever};
use stoer::query::subscript::{Subscript, SubscriptStep};
use stoer::query::{QueryFilter, TestOperation};
use stoer::reexport::wrapped_toml_edit::ItemOwned;

#[derive(Debug, Clone, clap::Parser)]
#[clap(after_long_help = QUERY_FILTERS)]
pub struct QueryArgs {
	/// A list of query filters, which are ANDed together.
	filters: Vec<OsString>,
}

impl QueryArgs {
	pub fn parse_filters(self) -> Result<Vec<QueryFilter>, Whatever> {
		let mut res = Vec::new();
		let mut args = self.filters.into_iter();
		while let Some(next) = args.next() {
			res.push(parse_query_filter(next, &mut args)?);
		}
		Ok(res)
	}
}

const QUERY_FILTERS: &str = "\x1b[1m\x1b[4mQuery Filters:\x1b[0m
- Test: \"?<param>\" <test operation>
	Performs a check on the value of <param>.
	Possible test operations:
	- ':?' or ':ex' - Test for existence.
	- '= <value>' or ':eq <value>' - Test for equality. \
	  The value has to be a TOML value \
	  and there can be any amout of whitespace before it
	  (The ':eq' form requires at least one space).
	- ':cont <value>' - Test for containment of <value> in an array.
- Pipe: '$' <params...> '|' <cmd> <prefix args...> ';'
	Extracts the values of <params> (each must be a string) and gives them as arguments to another process, \
	the exit status of which is used as the filter.
	The parameters are always given as the final arguments, after the <prefix args...>.";

fn parse_query_filter(
	next: OsString,
	args: &mut impl Iterator<Item = OsString>,
) -> Result<QueryFilter, Whatever> {
	let next = next
		.to_str()
		.whatever_context("invalid query filter (not UTF-8)")?;
	if next == "$" {
		let params = args
			.take_while(|s| s != "|")
			.map(|s| s.to_str().map(parse_subscript))
			.collect::<Option<Vec<_>>>()
			.whatever_context("invalid subscript (not UTF-8)")?;
		let mut prefix_args = args.take_while(|s| s != ";");
		let cmd = prefix_args
			.next()
			.whatever_context("missing command after '|'")?;
		let prefix_args = prefix_args.collect();
		Ok(QueryFilter::PipeOk {
			params,
			cmd,
			prefix_args,
		})
	} else if let Some(param_s) = next.strip_prefix('?') {
		let param = parse_subscript(param_s);
		let op_s = args
			.next()
			.whatever_context("missing test operation after '?...'")?;
		let op_s = op_s
			.to_str()
			.whatever_context("invalid test operation (not UFT-8)")?;
		let op = parse_test_operation(op_s)?;
		Ok(QueryFilter::Test(param, op))
	} else {
		whatever!("invalid query filter: {next:?}")
	}
}

fn parse_test_operation(s: &str) -> Result<TestOperation, Whatever> {
	use stoer::reexport::wrapped_toml_edit::toml_edit::Item;
	if s == ":?" || s == ":ex" {
		Ok(TestOperation::Exists)
	} else if let Some(rhs) = s.strip_prefix('=').or_else(|| s.strip_prefix(":eq ")) {
		let rhs = rhs.trim_start();
		let item = rhs
			.parse::<Item>()
			.with_whatever_context(|_| format!("invalid comparison value: {rhs}"))?;
		let item_handle = ItemOwned::new(item).expect("parse() returned Item::None");
		Ok(TestOperation::Eq(item_handle))
	} else if let Some(rhs) = s.strip_prefix(":cont ") {
		let rhs = rhs.trim_start();
		let item = rhs
			.parse::<Item>()
			.with_whatever_context(|_| format!("invalid comparison value: {rhs}"))?;
		let item_handle = ItemOwned::new(item).expect("parse() returned Item::None");
		Ok(TestOperation::ArrayContains(item_handle))
	} else {
		whatever!("invalid test operation: {s:?}")
	}
}

fn parse_subscript(s: &str) -> Subscript {
	let mut res = vec![];
	let mut y = String::new();
	for x in s.split('.') {
		let esc_count = x.len() - x.trim_end_matches('\\').len();
		if esc_count % 2 == 1 {
			y += &x[..x.len() - 1].replace(r"\\", r"\");
			y += ".";
			continue;
		}
		let x = x.replace(r"\\", r"\");
		let piece = if !y.is_empty() {
			y += &x;
			std::mem::take(&mut y)
		} else {
			x
		};
		let (piece, deref) = if let Some(piece) = piece.strip_suffix('@') {
			(Cow::Borrowed(piece), true)
		} else {
			(Cow::Owned(piece), false)
		};
		res.push(if let Ok(n) = piece.parse() {
			SubscriptStep::IndexOrField(n, piece.into_owned())
		} else {
			SubscriptStep::Field(piece.into_owned())
		});
		if deref {
			// TODO: syntax for explicitly specifying deref kind
			res.push(SubscriptStep::Deref(None));
		}
	}
	Subscript(res)
}
