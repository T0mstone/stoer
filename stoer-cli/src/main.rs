use clap::{Parser, Subcommand};
use directories::BaseDirs;
use snafu::{FromString, OptionExt, ResultExt, Whatever};
use stoer::storage::MultiDbTree;

type Errors<E> = outerr::ErrorSink<Vec<E>>;

mod query;

#[derive(Debug, Clone, Subcommand)]
enum Command {
	Query(query::QueryArgs),
}

#[derive(Debug, Parser)]
#[clap(
	infer_subcommands = true,
	after_help = "Note: Each command can be shortened to its minimal unique prefix."
)]
struct Args {
	#[clap(subcommand)]
	command: Command,
}

#[snafu::report]
fn main() -> Result<(), Whatever> {
	let Args { command } = Args::parse();

	match command {
		Command::Query(args) => {
			let filters = args.parse_filters()?;
			let mut dbs = load_dbs()?;

			let (res, query_errs) = Errors::new()
				.finish_with(|errs| {
					// TODO: querying of the other kinds
					stoer::query::query(&mut dbs, stoer::data::DataKind::Content, &filters, errs)
				})
				.map_err(|errs| {
					Whatever::without_source(format!(
						"malformed items: {}",
						errs.into_iter()
							.map(|it| { format!("{it:?}") })
							.collect::<Vec<_>>()
							.join(", ")
					))
				})?;

			for e in query_errs {
				eprintln!("error: {e}");
			}

			for (k, v) in res {
				println!("[{k}]\n{}", v);
			}
		}
	}
	Ok(())
}

fn load_dbs() -> Result<MultiDbTree, Whatever> {
	let base_dirs = BaseDirs::new().whatever_context("couldn't determine data directory")?;
	let data_dir = &base_dirs.data_dir().join("stoer");
	std::fs::create_dir_all(data_dir).whatever_context("couldn't create data directory")?;

	let mut dbs = MultiDbTree::new();
	Errors::new()
		.finish_with(|errs| dbs.read_dir_rec(data_dir, errs))
		.map_err(|errs| match <[_; 1]>::try_from(errs) {
			Ok([(path, err)]) => {
				Whatever::with_source(Box::new(err) as _, format!("error in {path:?}"))
			}
			Err(errs) => Whatever::without_source(format!(
				"multiple errors:\n{}",
				errs.into_iter()
					.map(|(path, err)| { format!("\tin {path:?}: {err}") })
					.collect::<Vec<_>>()
					.join("\n")
			)),
		})
		.ok()
		.map(|()| dbs)
		.whatever_context("invalid data")
}
