# Core Design

The Stör database lives in a single directory,
which is determined using the `directories` crate
(usually `~/.local/share/stoer` on Linux).

The basic structure is as follows:
```text
<root>
|- db.toml
|- bm.toml
|- types.toml
|- tags.toml
|- format.txt
\- plugin/
```
You can also have arbitrarily nested subdirectories containing the same structure.
These are scoped, so declaring something in a directory means you can only use it in that directory
and its subdirectories.

`format.txt` doesn't do anything, but it is explicitly part of the design,
so that you don't have to worry about Stör deleting a similar file you might put in there yourself otherwise.
Its only intended purpose is self-documentation of your database.

All the other files are [TOML](https://toml.io/en/)-based and operate on _entries_,
which are just top-level tables.

The key/value pairs of an entry are called _properties_.
All properties are optional unless otherwise mentioned.

## A note on TOML
TOML was chosen as a format because it's simple, familiar (for me, as a Rust developer),
_similar to a kind of database_, and most importantly: Human-readable and -editable with support for comments.

Stör will always try to preserve all comments it encounters.
(This is done via usage of the `toml_edit` crate, so I'm pretty confident that it will work without any issues.)


## Universal properties
These properties are supported by each type of entry.
- `meta` (arbitrary table): User-defined Properties.
	- This is intended for general properties not supported by Stör.
	- Stör will never touch this table, except for giving the user an interface to it.
	- Plugins can be allowed to access (parts of) this table.
- `plugin` (table of arbitrary tables): Plugin-defined Properties.
	- This is for properties that only make sense specific to a single plugin.
		For general properties that might also be used by other plugins, use `meta` instead.
	- The keys in the `plugin` table are plugin IDs (see below)
- `tags` (array of strings): List of tags that apply to this entry.
	- These can be tags as defined in a `tags.toml`, or just any other arbitrary text
		- There will be an option to give out warnings when encountering the latter (e.g. to protect against misspellings).

## Generic properties
These properties are supported by several types of entry (and listed without further details there).
- `name` (string): A human-readable name
- `desc` (string): A full-text description
- `parent` (string) / `children` (array of strings): Hierarchical structure.
	- Only one direction needs to be specified for a parent-child-connection.
		- There will be an option to automatically add the reverse directions when saving.

## `db.toml`
This is the main database file, containing the entries.

Valid properties:
- `name`: can also be an array of strings, for when an entry has multiple primary names
- `aliases` (list of strings): Human-readable secondary names.
- `desc`
- `parent`/`children`
- `type` (string): The type of entry this is.
	- This has to be one of the types declared in `types.toml`.
	- example values: `book`, `video`, `tv-series`, `blog`, `website`, or even `person`
	- There will be an option to give out warnings for all entries without a `type`.

## `types.toml`
This is the file declaring all the possible values for the `type` of a main entry.
Some basic types will be shipped with a default `types.toml` in something like `/usr/share/stoer/types.toml`.

Valid properties:
- `name`
- `desc`
- `supertype`/`subtypes`: Type hierarchies. Works exactly like `parent`/`children`
- `parent-types`/`children-types` (both arrays of strings): The allowed *types*
	of `parent`/`children` of entries of this type. Multiple values mean being able to choose.
- `bookmark` (schema): A schema describing what bookmarks to entries of this type have to look like.
	- example for `book`: `{ required = { page = "uint" }, optional = { chapter = "uint" } }`
	- There will be an option to give out warnings for all types without a `bookmark` schema.

## `bm.toml`
This file contains all bookmarks.

Valid properties:
- `for` (string): The key of the entry this bookmark points at
	- Bookmarks without this are called _ad-hoc_.
	- If you only have one bookmark for a given entry, it is recommended
		to store that bookmark with the same key as its targeted entry (i.e. its `for` value)
- `loc` (**required**<sup>\*</sup>, any): Where inside the entry this bookmark points
	- Which values this can take is given by the `bookmark` value of the `type` of the targeted entry.
		- <sup>\*</sup>) In case the bookmark has no allowed fields (i.e. the type has `bookmark = {}`),
			this property may be omitted (or specified as `loc = {}`)
- `locx` (any): Additional data to `loc` that doesn't fit the schema.
	- This is different from `meta` as it will be deleted when `loc` is written to.
- `after` (boolean): For discrete `loc` values:
	whether this bookmarks points to before or after the given location.
	- example: If the targeted entry is an episode of a TV series, this stores whether you have watched that episode.
	- This can always be given, but might not be useful,
		for example if the targeted entry is a video, then `loc` would be a timestamp and time is continuous,
		so there's no real difference.
	- If subsequent entries (say, `N` and `N+1`) exist, then a bookmark to `N` with `after = true`
		is equivalent to a bookmark to `N+1` with `after = false`.
		Therefore, `after` is most useful when the next location doesn't exist yet,
		such as for a currently premiering TV series.

## `tags.toml`
This defines a set of known tags, which can help with organization.

Valid properties:
- `desc`
- `parent`/`children`


## Plugins and `plugin/`
Plugins are a big planned feature to extend the capabilities of what you can do with your database.

### Plugin IDs
A plugin ID has three components:
- Namespace
- Name
- Version

While the version is required to match the regular expression `[0-9]+(\.[0-9]+)*`,
nothing else is enforced.
Stör will not concern itself with plugin _management_,
so it will just accept and work with whatever namespace plugins declare for themselves.

Different versions of the same plugin are mostly treated as completely different plugins,
but there will be an upgrade mechanism to migrate plugin-specific data from an older to a newer version.

### Plugin dependencies
Plugins can declare three types of dependencies:
- necessary dependencies
- optional dependencies (which will activate special compatibility code if present)
- extension dependencies
	- These are functionally identical to necessary dependencies,
		but the interpretation is different.
		Use these if your plugin is actually (somewhat of) an extension to the plugins you list here.

As stated above, Stör doesn't concern itself with plugin _management_,
so it will handle plugin dependencies similarly to, for example, the Minecraft Forge mod loader:
It trusts the plugins with what they say they are and only validates the dependency tree,
giving out an error if a necessary/extension dependency is missing.

<!-- note: there might be a separate package manager for plugins later, but that's very low priority -->

### Plugin functionality
Plugins define actions and services.
- Actions are one-time and triggered by the user
	- They have fixed inputs that can be exposed as CLI options (or as input fields in a UI)
- Services are automatic and triggered by an event
	- The service declares what kind of events it subscribes to

### Permission management
Plugins are strictly controlled when it comes to accessing the database.
- They need to explicitly declare any and all `meta` and `locx` keys they want access to.
	- These can be given a priority, which is just used as a notice to the user,
		who can deny any and all of them.
- The user can completely control, what entries a plugin can see.
For both of these, things that were denied to the plugin will just appear as non-existent:
The plugin won't be able to know what it was denied.
This is to prevent plugins from maliciously refusing to work unless you grant them certain permissions.

### Security
Plugins should also run in a sandbox, so the user's computer is never in danger.
WebAssembly will probably be a good choice.


### The `plugin/` folder
The `plugin/` folder contains whole plugin-specific databases.
Each subdirectory of `plugin/` carries the name of a plugin ID
(or a filesystem-friendly encoding of that),
the internal structure of which is completely up to the plugin.

