use std::fmt;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct PluginId<S> {
	/// The namespace of this plugin.
	///
	/// Usually, this will be the name of the
	/// author or organization responsibe for this plugin.
	pub namespace: S,
	/// The name of this plugin.
	pub name: S,
	/// The major version of this plugin.
	///
	/// Minor or patch versions don't matter to the database.
	/// Plugins must be written in a forward-compatible way,
	/// so that they can handle data from a backwards-compatible future version of themselves.
	/// Note that a "backwards-compatible future version" is determined by having the same major version.
	pub version: u64,
}

/// Encode a string into a mangled form:
/// - If the string doesn't contain the character `0`, it is represented verbatim.
/// - Otherwise, if the string doesn't start with an ASCII digit or the character `_`,
///   it is prefixed by the number of `0` characters it contains,
///   formatted in such a way as to begin with a zero.
///   Note that this number of zeros is at most a 64-bit unsigned integer.
/// - Otherwise, if the string does start with an ASCII digit or the character `_`,
///   it is prefixed with a `_` and then prefixed like in the previous point.
///
/// In this way, the mangled string always matches the regex `0(?:[1-9][0-9]*)?_?(.+)`,
/// where the capturing group contains the unmangled string.
/// Concatenating this with another such mangled string is a fully reversible operation
/// since the leading `0` of the second one can be determined to not be part of the first string due to its prefix.
// NOTE: This specific encoding was designed to produce nice-looking results for typical inputs.
struct ZeroEnc<'a>(&'a str);

impl fmt::Display for ZeroEnc<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let s = self.0;
		let zeros = s.bytes().filter(|b| *b == b'0').count();
		if zeros == 0 {
			f.write_str(s)
		} else {
			let sep = if s
				.bytes()
				.next()
				.is_some_and(|b| b.is_ascii_digit() || b == b'_')
			{
				"_"
			} else {
				""
			};
			f.write_fmt(format_args!("0{zeros}{sep}{s}"))
		}
	}
}

impl<'a> ZeroEnc<'a> {
	pub fn split_prefix(s: &mut &'a str) -> Self {
		if s.starts_with('0') {
			let mut offset = 1;
			let mut zeros = 0;
			for c in s.bytes().skip(1).take_while(|b| b.is_ascii_digit()) {
				offset += 1;
				zeros *= 10;
				zeros += (c - b'0') as u64;
			}
			*s = &s[offset..];
			if s.starts_with('_') {
				*s = &s[1..];
			}
			let mut seen_zeros = 0;
			let mut prefix_len = 0;
			for b in s.bytes() {
				if b == b'0' {
					if seen_zeros == zeros {
						break;
					}
					seen_zeros += 1;
				}
				prefix_len += 1;
			}
			let (prefix, rest) = s.split_at(prefix_len);
			*s = rest;
			Self(prefix)
		} else {
			// no `0` at the start means that the unmangled string doesn't contain a `0`,
			// so just take until the next `0`.
			let (prefix, rest) = s.split_once('0').unwrap_or((s, ""));
			*s = rest;
			Self(prefix)
		}
	}
}

impl<S> PluginId<S> {
	/// This is the plugin key used to identify this plugin in e.g. the plugin-specific properties
	pub fn to_key(&self) -> String
	where
		S: AsRef<str>,
	{
		format!(
			"{}{}0{}",
			ZeroEnc(self.namespace.as_ref()),
			ZeroEnc(self.name.as_ref()),
			self.version
		)
	}

	/// Construct a `PluginId` from a [plugin key](Self::to_key)
	pub fn from_key(mut key: &str) -> Option<Self>
	where
		S: for<'a> From<&'a str>,
	{
		let namespace = ZeroEnc::split_prefix(&mut key).0;
		let name = ZeroEnc::split_prefix(&mut key).0;
		debug_assert_eq!(key.bytes().next(), Some(b'0'));
		let version = key[1..].parse().ok()?;

		Some(Self {
			namespace: namespace.into(),
			name: name.into(),
			version,
		})
	}
}
