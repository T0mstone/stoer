use either::Either;
use wrapped_toml_edit::validate::wrappers::IsArrayValidate;
use wrapped_toml_edit::validate::{IsAny, IsPrimitive, IsTable};

pub mod plugin_specific {
	use perfect_derive::perfect_derive;
	use wrapped_toml_edit::handle::{
		FromOwned, GenericHandleHKT, GenericHandleKind, GenericHandleToOwned, Mut,
	};
	use wrapped_toml_edit::validate::wrappers::{
		IsTableValidate, PropResult, ValidatingTableEntry, ValidatingTableHandle,
		ValidatingTableHandleHKT,
	};
	use wrapped_toml_edit::validate::{IsTable, MalformedItem, ValidateItem};
	use wrapped_toml_edit::{ItemHandle, ItemOwned, TableMut, TableOwned};

	use crate::plugin::PluginId;

	#[perfect_derive(Debug)]
	pub struct PluginSpecificTableHandle<'a, K: GenericHandleKind>(
		pub ValidatingTableHandle<'a, IsTable, K>,
	);

	impl<K: GenericHandleKind + FromOwned> Default for PluginSpecificTableHandle<'_, K> {
		fn default() -> Self {
			Self(ValidatingTableHandle::default())
		}
	}

	impl<K: GenericHandleKind> PluginSpecificTableHandle<'_, K> {
		pub fn as_mut(&mut self) -> PluginSpecificTableHandle<Mut> {
			PluginSpecificTableHandle(self.0.as_mut())
		}
	}

	impl<'a> PluginSpecificTableHandle<'a, Mut> {
		pub fn get_mut(self, id: PluginId<&str>) -> PropResult<'a, TableMut<'a>> {
			let key = id.to_key();

			self.0.get_mut(&key)
		}

		pub fn entry(self, id: PluginId<&str>) -> ValidatingTableEntry<'a, IsTable> {
			let key = id.to_key();

			self.0.entry(&key)
		}

		pub fn try_insert<'o>(
			self,
			id: PluginId<&str>,
			table: TableOwned<'o>,
		) -> Result<(), ItemOwned<'o>> {
			let key = id.to_key();

			self.0.try_insert(&key, table)
		}
	}

	pub enum PluginSpecificTableHandleHKT {}

	impl GenericHandleHKT for PluginSpecificTableHandleHKT {
		type Handle<'a, K: GenericHandleKind> = PluginSpecificTableHandle<'a, K>;

		fn cast_owned_lt<'b>(
			handle: Self::Handle<'_, wrapped_toml_edit::handle::Owned>,
		) -> Self::Handle<'b, wrapped_toml_edit::handle::Owned> {
			PluginSpecificTableHandle(ValidatingTableHandleHKT::cast_owned_lt(handle.0))
		}

		fn as_mut<'a, K: GenericHandleKind>(
			handle: &'a mut Self::Handle<'_, K>,
		) -> Self::Handle<'a, Mut> {
			PluginSpecificTableHandle(ValidatingTableHandleHKT::as_mut(&mut handle.0))
		}

		fn into_dynamic<K: GenericHandleKind>(
			handle: Self::Handle<'_, K>,
		) -> Self::Handle<'_, wrapped_toml_edit::handle::Dynamic> {
			PluginSpecificTableHandle(ValidatingTableHandleHKT::into_dynamic(handle.0))
		}
	}
	impl GenericHandleToOwned for PluginSpecificTableHandleHKT {
		fn into_owned<'b, K: GenericHandleKind>(
			handle: Self::Handle<'_, K>,
		) -> Self::Handle<'b, wrapped_toml_edit::handle::Owned> {
			PluginSpecificTableHandle(ValidatingTableHandleHKT::into_owned(handle.0))
		}

		fn to_owned<'b, K: GenericHandleKind>(
			handle: &Self::Handle<'_, K>,
		) -> Self::Handle<'b, wrapped_toml_edit::handle::Owned> {
			PluginSpecificTableHandle(ValidatingTableHandleHKT::to_owned(&handle.0))
		}
	}

	pub enum IsPluginSpecificTable {}

	impl ValidateItem for IsPluginSpecificTable {
		type Output = PluginSpecificTableHandleHKT;

		fn validate<K: GenericHandleKind>(
			item: ItemHandle<K>,
		) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
			IsTableValidate::validate(item).map(PluginSpecificTableHandle)
		}

		#[inline]
		fn unvalidate<K: GenericHandleKind>(
			item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
		) -> ItemHandle<K> {
			IsTableValidate::unvalidate(item.0)
		}
	}
}

macro_rules! db_entry {
	($(#[$($attr:tt)*])*
	$v:vis struct $name:ident {
		$($fields:tt)*
	}
	$($rest:tt)*) => {
		wrapped_toml_edit::table_struct! {
			$(#[$($attr)*])*
			$v struct $name {
				/// Plugin-specific properties.
				plugin_specific "plugin": plugin_specific::IsPluginSpecificTable,
				/// User-defined properties.
				meta: IsTable,
				/// Tags.
				tags: IsArrayValidate<IsPrimitive<String>>,
				$($fields)*
			}
			$($rest)*
		}
	};
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum DataKind {
	Content,
	ContentType,
	Bookmark,
	Tag,
}

impl std::fmt::Display for DataKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.pad(match self {
            DataKind::Content => "content",
            DataKind::ContentType => "type",
            DataKind::Bookmark => "bookmark",
            DataKind::Tag => "tag",
        })
    }
}

db_entry! {
	/// A content database entry
	pub struct ContentHandle {
		/// The official name or names of the content.
		name: Either<IsPrimitive<String>, IsArrayValidate<IsPrimitive<String>>>,
		/// Secondary names this content is known by.
		aliases: IsArrayValidate<IsPrimitive<String>>,
		/// A description of the content.
		desc: IsPrimitive<String>,
		/// The (key of the) content this content is a part of.
		parent: IsPrimitive<String>,
		/// The (keys of the) contents that are part of this content.
		children: IsArrayValidate<IsPrimitive<String>>,
		/// The (key of the) type of content this is.
		type_ "type": IsPrimitive<String>
	}
	pub use @{
		hkt as ContentHandleHKT,
		validate as IsContent,
		split as Content
	}
}

db_entry! {
	/// A content type database entry
	pub struct ContentTypeHandle {
		/// The name of the content type.
		name: IsPrimitive<String>,
		/// A description of the content type.
		desc: IsPrimitive<String>,
		/// The type that parent content of content with this type has.
		parent: IsPrimitive<String>,
		/// The possible types of child content of content with this type.
		children: IsArrayValidate<IsPrimitive<String>>,
		/// A larger type that contains all content of this type and possibly more.
		supertype: IsPrimitive<String>,
		/// Types to which this type is a direct supertype.
		subtype: IsArrayValidate<IsPrimitive<String>>,
		// todo: TypeSchema
		/// What bookmarks for content of this type look like.
		bookmark: IsTable
	}
	pub use @{
		hkt as ContentTypeHandleHKT,
		validate as IsContentType,
		split as ContentType
	}
}

db_entry! {
	/// A bookmark database entry
	pub struct BookmarkHandle {
		/// The (key of the) content that the bookmark is at.
		for_ "for": IsPrimitive<String>,
		/// The location inside `for` that the bookmark is at.
		// TODO: auto-inserted default value `ItemHandle::Table(TableHandle::InlineTable(Default::default()))`
		loc: IsAny,
		/// Auxilliary location data that doesn't fit into the schema for `loc`.
		locx: IsAny,
		/// Whether the bookmark is just after the specified position (or just before it).
		///
		/// Intended for discrete locations (e.g. pages in a book).
		/// Doesn't always make sense (e.g. when `loc` is a timestamp).
		after: IsPrimitive<bool>,
	}
	pub use @{
		hkt as BookmarkHandleHKT,
		validate as IsBookmark,
		split as Bookmark
	}
}

db_entry! {
	/// A tag database entry
	pub struct TagHandle {
		/// A description of the tag.
		desc: IsPrimitive<String>,
		/// A larger tag that contains all objects with this tag and possibly more.
		parent: IsPrimitive<String>,
		/// Tags to which this tag is a direct parent.
		children: IsArrayValidate<IsPrimitive<String>>,
	}
	pub use @{
		hkt as TagHandleHKT,
		validate as IsTag,
		split as Tag
	}
}
