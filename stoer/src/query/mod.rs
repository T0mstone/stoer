use std::collections::{BTreeMap, HashMap, HashSet};
use std::ffi::OsString;

use outerr::{ErrorMonoid, ErrorSink, ResultExt as _};
use snafu::{ResultExt, Snafu};
use subscript::{ExpectedType, Subscript, SubscriptError};
use wrapped_toml_edit::{ItemHandle, ItemMut, ItemOwned, PrimitiveHandle, ValueEq};

use crate::data::DataKind;
use crate::storage::{DbError, MultiDbTree, OwningReference, Reference};

pub mod subscript;

pub fn query<'a>(
	tree: &'a mut MultiDbTree,
	kind: DataKind,
	filters: &[QueryFilter],
	db_errs: &mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
) -> (BTreeMap<String, ItemMut<'a>>, Vec<QueryError>) {
	// Yes, iterating twice is not very nice, but it gets the job done.
	// Might need to optimize later, but it works for now.
	let mut query_errs = ErrorSink::<Vec<QueryError>>::new();
	let allowed = tree
		.erased_iter_mut(kind, db_errs)
		.map(|(p, k, _)| (p.to_path_buf(), k.to_string()))
		.collect::<Vec<_>>()
		.into_iter()
		.filter(|(scope, key)| {
			let r = Reference { kind, scope, key };
			filters.iter().all(|f| {
				query_filter(tree, r, f, db_errs)
					.err_to(&mut query_errs)
					.is_some_and(|b| b)
			})
		})
		.fold(HashMap::<_, HashSet<_>>::new(), |mut acc, (p, s)| {
			acc.entry(p).or_default().insert(s);
			acc
		});

	(
		// discard the errors since they were already emitted
		tree.erased_iter_mut(kind, &mut ErrorSink::<Vec<DbError>>::new())
			.filter(|(p, k, _)| allowed.get(*p).is_some_and(|s| s.contains(&**k)))
			.map(|(_, k, it)| (k.to_string(), it))
			.collect(),
		query_errs.0,
	)
}

/// A filter to evaluate on an entry.
#[derive(Debug)]
pub enum QueryFilter {
	/// Fetch a property via the subscript mechanism and perform some check on it.
	Test(Subscript, TestOperation),
	/// Execute a command with the values of subscript expressions at the given entry
	/// and use its exit status as a filter.
	///
	/// If the subscript expressions don't exist, this will just evaluate to false,
	/// just like with `Test(_, Exists)`.
	PipeOk {
		params: Vec<Subscript>,
		cmd: OsString,
		prefix_args: Vec<OsString>,
	},
	// MaybeTODO: OR, NOT, parentheses (or maybe use rust-cfg-like `--any, --not`)
}

#[derive(Debug, Clone)]
pub enum TestOperation {
	/// Whether the subscript expression can even be applied to the given entry.
	Exists,
	/// Whether the subscript expression evaluates to the given value.
	/// Implicitly includes the `Exists` check.
	Eq(ItemOwned<'static>),
	/// Whether the subscript expression evaluates to an array containing the given value.
	/// Implicitly includes the `Exists` check.
	ArrayContains(ItemOwned<'static>),
}

pub fn test_item(item: Option<ItemMut>, test: &TestOperation) -> bool {
	match test {
		TestOperation::Exists => item.is_some(),
		TestOperation::Eq(it) => item.is_some_and(|item| item.value_eq(it)),
		TestOperation::ArrayContains(it) => item.is_some_and(|item| match item {
			ItemHandle::Array(a) => a.iter_mut().any(|elt| elt.value_eq(it)),
			_ => false,
		}),
	}
}

#[derive(Debug, Snafu)]
#[snafu(context(suffix(false)), module(error))]
pub enum QueryError {
	/// The original reference is invalid.
	InvalidReference { r: OwningReference },
	/// A subscript expression couldn't be applied due to a type mismatch.
	SubscriptTypeError {
		entry: OwningReference,
		subscript: Subscript,
		item: ItemOwned<'static>,
		expected: ExpectedType,
	},
	/// A pipe filter's argument turned out to not be a string.
	PipeArgNotString {
		index: usize,
		entry: OwningReference,
		subscript: Subscript,
		item: ItemOwned<'static>,
	},
	/// Failed to execute command for `PipeOk`.
	ExecuteCommand { source: std::io::Error },
}

pub fn query_filter(
	tree: &mut MultiDbTree,
	r: Reference,
	filter: &QueryFilter,
	db_errs: &mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
) -> Result<bool, QueryError> {
	match filter {
		QueryFilter::Test(subscript, test) => {
			let item = query_subscript(tree, r, subscript, db_errs)?;
			Ok(test_item(item, test))
		}
		QueryFilter::PipeOk {
			params,
			cmd,
			prefix_args,
		} => params
			.iter()
			.enumerate()
			.map(|(index, param_subscript)| {
				query_subscript(tree, r, param_subscript, db_errs)?
					.map(|item| {
						item.into_primitive()
							.and_then(|prim| match prim {
								PrimitiveHandle::String(s) => Ok(s.value().to_string()),
								_ => Err(ItemHandle::Primitive(prim)),
							})
							.map_err(|item| QueryError::PipeArgNotString {
								index,
								entry: r.to_owning(),
								subscript: param_subscript.clone(),
								item: item.into_owned(),
							})
					})
					.transpose()
			})
			.collect::<Result<Option<Vec<_>>, _>>()?
			.map(|params| {
				std::process::Command::new(cmd)
					.args(prefix_args)
					.args(params)
					.status()
			})
			.transpose()
			.context(error::ExecuteCommand)
			.map(|opt_status| opt_status.is_some_and(|status| status.success())),
	}
}

/// Error handling adapter to `subscript::subscript`.
fn query_subscript<'a>(
	tree: &'a mut MultiDbTree,
	r: Reference,
	subscript: &Subscript,
	db_errs: &mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
) -> Result<Option<ItemMut<'a>>, QueryError> {
	match subscript::subscript(tree, r, subscript, db_errs) {
		Ok(it) => Ok(Some(it)),
		Err(
			SubscriptError::FieldInvalid { .. }
			| SubscriptError::IndexInvalid { .. }
			| SubscriptError::DerefInvalid { .. },
		) => Ok(None),
		Err(SubscriptError::FirstRefInvalid { r }) => Err(QueryError::InvalidReference { r }),
		Err(SubscriptError::TypeError {
			entry,
			subscript,
			item,
			expected,
		}) => Err(QueryError::SubscriptTypeError {
			entry,
			subscript,
			item,
			expected,
		}),
	}
}
