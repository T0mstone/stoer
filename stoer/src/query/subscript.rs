use outerr::{ErrorMonoid, ErrorSink};
use snafu::Snafu;
use wrapped_toml_edit::{ItemHandle, ItemMut, ItemOwned, PrimitiveHandle};

use crate::data::DataKind;
use crate::storage::{DbError, MultiDbTree, OwningReference, Reference};

/// A path from an entry to some value.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Subscript(pub Vec<SubscriptStep>);

/// A single-step [`Subscript`].
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum SubscriptStep {
	/// Access the given field. Fails if called on something other than a table.
	Field(String),
	/// Access the given field (if called on a table) or index (if called on an array). Fails if called on a primitive.
	IndexOrField(usize, String),
	/// Interpret the called-on string as an ID and access the entry of that ID. Fails if called on a non-string.
	// TODO: Auto-detects the data kind of ID to use based on the surrounding context.
	// -> key `type` links to `DataKind::ContentType`, key `tags` links to `DataKind::Tag`, etc.
	// Idea: keep track of "well known keys" encountered while stepping; Each one of them sets a context flag that enables derefing.
	// -> For other things (where it can't be inferred from context), require an explicit data kind
	Deref(Option<DataKind>),
}

#[derive(Debug, Snafu)]
#[snafu(context(suffix(false)), module(error))]
pub enum SubscriptError<E = OwningReference, S = Subscript> {
	/// Tried to perform a subscript step on an invalid type.
	TypeError {
		entry: E,
		subscript: S,
		item: ItemOwned<'static>,
		expected: ExpectedType,
	},
	/// Tried to access a non-existent field on a table.
	FieldInvalid { entry: E, subscript: S },
	/// Tried to access an array at an index out of bounds.
	IndexInvalid { entry: E, subscript: S },
	/// Tried to access an invalid reference via a deref operation.
	DerefInvalid {
		/// The entry the reference string is on
		entry: E,
		/// The subscript describing (referencing) the reference string
		subscript: S,
		/// The reference string's resolved value
		r: OwningReference,
	},
	/// Tried to access an invalid reference to begin with.
	FirstRefInvalid { r: OwningReference },
}

#[derive(Debug)]
pub enum ExpectedType {
	Table,
	TableOrArray,
	String,
}

impl<E, S> SubscriptError<E, S> {
	fn map<F, T>(self, f: impl FnOnce(E) -> F, g: impl FnOnce(S) -> T) -> SubscriptError<F, T> {
		match self {
			SubscriptError::TypeError {
				entry,
				subscript,
				item,
				expected,
			} => SubscriptError::TypeError {
				entry: f(entry),
				subscript: g(subscript),
				item,
				expected,
			},
			SubscriptError::FieldInvalid { entry, subscript } => SubscriptError::FieldInvalid {
				entry: f(entry),
				subscript: g(subscript),
			},
			SubscriptError::IndexInvalid { entry, subscript } => SubscriptError::IndexInvalid {
				entry: f(entry),
				subscript: g(subscript),
			},
			SubscriptError::DerefInvalid {
				entry,
				subscript,
				r,
			} => SubscriptError::DerefInvalid {
				entry: f(entry),
				subscript: g(subscript),
				r,
			},
			SubscriptError::FirstRefInvalid { r } => SubscriptError::FirstRefInvalid { r },
		}
	}
}

/// An intermediate result from a [`Subscript`], where [`Deref`](SubscriptOperation::Deref) hasn't been resolved.
enum Target<'a> {
	Direct(ItemMut<'a>),
	Deref(&'a str, Option<DataKind>),
}

fn subscript_step<'a>(
	item: ItemMut<'a>,
	step: &SubscriptStep,
) -> Result<Target<'a>, SubscriptError<(), ()>> {
	match step {
		SubscriptStep::Field(key) => {
			let table = item
				.into_table()
				.map_err(|item| SubscriptError::TypeError {
					entry: (),
					subscript: (),
					item: item.into_owned(),
					expected: ExpectedType::Table,
				})?;

			table
				.get_mut(key)
				.ok_or_else(|| SubscriptError::FieldInvalid {
					entry: (),
					subscript: (),
				})
				.map(Target::Direct)
		}
		&SubscriptStep::IndexOrField(idx, ref key) => match item {
			ItemMut::Array(array) => array
				.get_mut(idx)
				.ok_or_else(|| SubscriptError::IndexInvalid {
					entry: (),
					subscript: (),
				})
				.map(Target::Direct),
			ItemMut::Table(table) => table
				.get_mut(key)
				.ok_or_else(|| SubscriptError::FieldInvalid {
					entry: (),
					subscript: (),
				})
				.map(Target::Direct),
			item => Err(SubscriptError::TypeError {
				entry: (),
				subscript: (),
				item: item.into_owned(),
				expected: ExpectedType::TableOrArray,
			}),
		},
		SubscriptStep::Deref(kind) => {
			let s = deref_step(item)?;

			Ok(Target::Deref(s, *kind))
		}
	}
}

fn deref_step(item: ItemMut) -> Result<&str, SubscriptError<(), ()>> {
	item.into_primitive()
		.and_then(|prim| match prim {
			PrimitiveHandle::String(s) => Ok(s.value().as_str()),
			_ => Err(ItemHandle::Primitive(prim)),
		})
		.map_err(|item| SubscriptError::TypeError {
			entry: (),
			subscript: (),
			item: item.into_owned(),
			expected: ExpectedType::String,
		})
}

// note: Lifetime issues made this somewhat difficult to come up with.
//       First, we only borrow `tree` with a short lifetime to get to the final deref (if any);
//       Only then, at the end, can we use the lifetime `'a` to get the final result.
//       (Otherwise it would conflict with the earlier borrow.)
pub fn subscript<'a>(
	tree: &'a mut MultiDbTree,
	r: Reference,
	subscript: &Subscript,
	db_errs: &mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
) -> Result<ItemMut<'a>, SubscriptError> {
	let final_reference;
	let final_direct_steps;
	let (_, mut item) = match subscript.split_final_deref() {
		None => {
			final_reference = r.to_owning();
			final_direct_steps = subscript.0.as_slice();
			tree.get_mut(r, db_errs)
				.ok_or_else(|| SubscriptError::FirstRefInvalid {
					r: final_reference.clone(),
				})?
		}
		Some((steps, final_deref_kind, fds)) => {
			final_direct_steps = fds;
			let mut curr_ref = r.to_owning();
			// note: while `curr_ref` contains a lower bound on the path to look for,
			// `curr_path` always contains the exact file the current entry is in.
			let (mut curr_path, mut curr) =
				tree.get_mut(r, db_errs)
					.ok_or_else(|| SubscriptError::FirstRefInvalid {
						r: curr_ref.clone(),
					})?;

			// starting index of the current local step list
			let mut i0 = 0;
			let mut curr_direct_steps = &steps[0..0];
			for i in 0..steps.len() {
				let step = &steps[i];
				curr_direct_steps = &steps[i0..=i];

				let target = subscript_step(curr, step).map_err(|e| {
					e.map(
						|()| curr_ref.clone(),
						|()| Subscript(curr_direct_steps.to_vec()),
					)
				})?;
				match target {
					Target::Direct(it) => {
						curr = it;
					}
					Target::Deref(key, kind) => {
						let prev_ref = curr_ref;
						curr_ref = Reference {
							// TODO: actually determine kind in case of `None`
							kind: kind.unwrap_or(DataKind::Content),
							scope: curr_path,
							key,
						}
						.to_owning();
						(curr_path, curr) =
							tree.get_mut(curr_ref.borrow(), db_errs).ok_or_else(|| {
								SubscriptError::DerefInvalid {
									entry: prev_ref,
									subscript: Subscript(curr_direct_steps.to_vec()),
									r: curr_ref.clone(),
								}
							})?;
						i0 = i + 1;
					}
				}
			}
			let key = deref_step(curr).map_err(|e| {
				e.map(
					|()| curr_ref.clone(),
					|()| Subscript(curr_direct_steps.to_vec()),
				)
			})?;

			final_reference = Reference {
				// TODO: same as above
				kind: final_deref_kind.unwrap_or(DataKind::Content),
				scope: curr_path,
				key,
			}
			.to_owning();
			tree.get_mut(final_reference.borrow(), db_errs)
				.ok_or_else(|| SubscriptError::DerefInvalid {
					entry: curr_ref,
					subscript: Subscript(curr_direct_steps.to_vec()),
					r: final_reference.clone(),
				})?
		}
	};

	for i in 0..final_direct_steps.len() {
		let step = &final_direct_steps[i];
		let curr_direct_steps = &final_direct_steps[..=i];

		// note: there are no derefs in `final_steps`
		let Target::Direct(it) = subscript_step(item, step).map_err(|e| {
			e.map(
				|()| final_reference.clone(),
				|()| Subscript(curr_direct_steps.to_vec()),
			)
		})?
		else {
			unreachable!()
		};
		item = it;
	}

	Ok(item)
}

impl Subscript {
	fn split_final_deref(&self) -> Option<(&[SubscriptStep], Option<DataKind>, &[SubscriptStep])> {
		self.0.iter().enumerate().rev().find_map(|(i, step)| {
			if let SubscriptStep::Deref(k) = step {
				Some((&self.0[..i], *k, &self.0[i + 1..]))
			} else {
				None
			}
		})
	}
}
