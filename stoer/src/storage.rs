//! The storage structure

use std::collections::{BTreeMap, HashMap, VecDeque};
use std::path::{Path, PathBuf};

use outerr::{ErrorMonoid, ErrorSink, ResultExt as _};
use perfect_derive::perfect_derive;
use snafu::{ResultExt, Snafu};
use wrapped_toml_edit::toml_edit::{DocumentMut, KeyMut};
use wrapped_toml_edit::validate::ValidateItem;
use wrapped_toml_edit::validate::wrappers::{ValidOutputMut, ValidatingDocument};

use crate::data::{DataKind, IsBookmark, IsContent, IsContentType, IsTag};

#[derive(Default, Debug)]
pub struct MultiDbTree {
	pub content: ValidatingDocumentTree<IsContent>,
	pub types: ValidatingDocumentTree<IsContentType>,
	pub bookmarks: ValidatingDocumentTree<IsBookmark>,
	pub tags: ValidatingDocumentTree<IsTag>,
}

impl MultiDbTree {
	#[inline]
	pub fn new() -> Self {
		Self::default()
	}
}

/// A nested directory structure of one type of file
// TODO: maybe validate the documents immediately when reading them?
// -> could then get rid of ValidatingDocument and have V::Output here instead
#[perfect_derive(Debug)]
pub struct ValidatingDocumentTree<V: ValidateItem>(pub BTreeMap<PathBuf, ValidatingDocument<V>>);

impl<V: ValidateItem> Default for ValidatingDocumentTree<V> {
	fn default() -> Self {
		Self(BTreeMap::new())
	}
}

impl<V: ValidateItem> ValidatingDocumentTree<V> {
	#[inline]
	pub fn new() -> Self {
		Self::default()
	}
}

/*==== READ/WRITE ====*/

#[derive(Debug, Snafu)]
#[snafu(module, context(suffix(false)))]
pub enum ReadError {
	/// I/O error
	Io { source: std::io::Error },
	/// Invalid TOML
	Toml {
		source: wrapped_toml_edit::toml_edit::TomlError,
	},
}

#[inline]
fn open_doc(
	path: impl AsRef<Path> + Into<PathBuf>,
) -> Result<Option<DocumentMut>, (PathBuf, ReadError)> {
	fn inner(path: &Path) -> Result<Option<DocumentMut>, ReadError> {
		if !path.is_file() {
			return Ok(None);
		}

		std::fs::read_to_string(path)
			.context(read_error::Io)?
			.parse::<DocumentMut>()
			.context(read_error::Toml)
			.map(Some)
	}

	inner(path.as_ref()).map_err(|e| (path.into(), e))
}

impl<V: ValidateItem> ValidatingDocumentTree<V> {
	pub fn write_all(
		&self,
		path: impl AsRef<Path>,
		errs: &mut ErrorSink<impl ErrorMonoid<Error = (PathBuf, std::io::Error)>>,
	) {
		for (p_rel, x) in &self.0 {
			let p_abs = if p_rel.iter().count() == 0 {
				path.as_ref().to_path_buf()
			} else {
				path.as_ref().join(p_rel)
			};

			let _ = std::fs::write(&p_abs, x.inner.to_string())
				.map_err(|e| (p_abs, e))
				.err_to(errs);
		}
	}
}

impl MultiDbTree {
	const FILE_NAMES: [&'static str; 4] = ["db.toml", "types.toml", "bm.toml", "tags.toml"];

	// todo: better error context
	pub fn read_dir_rec(
		&mut self,
		path: impl AsRef<Path>,
		errs: &mut ErrorSink<impl ErrorMonoid<Error = (PathBuf, ReadError)>>,
	) {
		let mut queue = VecDeque::new();
		queue.push_back(path.as_ref().to_path_buf());

		while let Some(curr_path) = queue.pop_front() {
			let [cp, tp, bp, gp] = Self::FILE_NAMES.map(|name| curr_path.join(name));

			if let Some(doc) = open_doc(cp).err_to(errs).flatten() {
				self.content.0.insert(
					curr_path.strip_prefix(&path).unwrap().to_path_buf(),
					ValidatingDocument::new(doc),
				);
			}
			if let Some(doc) = open_doc(tp).err_to(errs).flatten() {
				self.types.0.insert(
					curr_path.strip_prefix(&path).unwrap().to_path_buf(),
					ValidatingDocument::new(doc),
				);
			}
			if let Some(doc) = open_doc(bp).err_to(errs).flatten() {
				self.bookmarks.0.insert(
					curr_path.strip_prefix(&path).unwrap().to_path_buf(),
					ValidatingDocument::new(doc),
				);
			}
			if let Some(doc) = open_doc(gp).err_to(errs).flatten() {
				self.tags.0.insert(
					curr_path.strip_prefix(&path).unwrap().to_path_buf(),
					ValidatingDocument::new(doc),
				);
			}

			for dir in std::fs::read_dir(&curr_path)
				.context(read_error::Io)
				.map_err(|e| (curr_path.to_owned(), e))
				.err_to(errs)
				.into_iter()
				.flatten()
			{
				if let Some(dir) = dir
					.context(read_error::Io)
					.map_err(|e| (curr_path.to_owned(), e))
					.err_to(errs)
				{
					if dir
						.file_type()
						.context(read_error::Io)
						.map_err(|e| (dir.path().to_owned(), e))
						.err_to(errs)
						.is_some_and(|ft| ft.is_dir())
					{
						queue.push_back(dir.path());
					}
				}
			}
		}
	}

	pub fn write_all(
		&self,
		path: impl AsRef<Path>,
		errs: &mut ErrorSink<impl ErrorMonoid<Error = (PathBuf, std::io::Error)>>,
	) {
		let [cp, tp, bp, gp] = Self::FILE_NAMES.map(|name| path.as_ref().join(name));

		self.content.write_all(cp, errs);
		self.types.write_all(tp, errs);
		self.bookmarks.write_all(bp, errs);
		self.tags.write_all(gp, errs);
	}
}

/*==== ACCESS ====*/

#[derive(Debug, Snafu)]
#[snafu(module, context(suffix(false)))]
pub enum DbError {
	#[snafu(display("Invalid item for key {key:?} in {path:?} ({item:?})"))]
	InvalidValue {
		path: PathBuf,
		key: String,
		item: wrapped_toml_edit::toml_edit::Item,
	},
	#[snafu(display("Key {key:?} repeated in {path:?} (already defined in {prev_path:?})"))]
	RepeatedKey {
		key: String,
		path: PathBuf,
		prev_path: PathBuf,
	},
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct OwningReference<K = DataKind> {
	pub kind: K,
	pub scope: PathBuf,
	pub key: String,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Reference<'a, K = DataKind> {
	pub kind: K,
	pub scope: &'a Path,
	pub key: &'a str,
}

impl<K: Copy> OwningReference<K> {
	pub fn borrow(&self) -> Reference<K> {
		Reference {
			kind: self.kind,
			scope: &self.scope,
			key: &self.key,
		}
	}
}

impl<'a, K> Reference<'a, K> {
	fn erase(self) -> Reference<'a, ()> {
		Reference {
			kind: (),
			scope: self.scope,
			key: self.key,
		}
	}

	pub fn to_owning(self) -> OwningReference<K> {
		OwningReference {
			kind: self.kind,
			scope: self.scope.to_path_buf(),
			key: self.key.to_string(),
		}
	}
}

impl MultiDbTree {
	pub fn get_mut<'a>(
		&'a mut self,
		r: Reference,
		errs: &mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
	) -> Option<(&'a Path, wrapped_toml_edit::ItemMut<'a>)> {
		macro_rules! get_mut_erased {
			($e:expr, $V:ident) => {
				$e.get_mut(r.erase(), errs)
					.map(|(p, h)| (p, $V::unvalidate(h)))
			};
		}

		match r.kind {
			DataKind::Content => get_mut_erased!(self.content, IsContent),
			DataKind::ContentType => get_mut_erased!(self.types, IsContentType),
			DataKind::Bookmark => get_mut_erased!(self.bookmarks, IsBookmark),
			DataKind::Tag => get_mut_erased!(self.tags, IsTag),
		}
	}
}

impl<V: ValidateItem> ValidatingDocumentTree<V> {
	pub fn get_mut(
		&mut self,
		r: Reference<()>,
		errs: &mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
	) -> Option<(&Path, ValidOutputMut<V>)> {
		// note: iterate from least to most specific path (from ancestor to descendant)
		let mut iter = self.0.iter_mut().filter_map(|(p, doc)| {
			if !r.scope.starts_with(p) {
				// only parent dirs of scope are allowed
				return None;
			}

			let doc = doc.as_table_mut();

			doc.get_mut(r.key)
				.map_err(|e| DbError::InvalidValue {
					path: p.to_path_buf(),
					key: r.key.to_string(),
					item: e.0.into_owned().into_item(),
				})
				.err_to(errs)
				.flatten()
				.map(|x| (p.as_path(), x))
		});

		let res = iter.next();

		// There's no harm in just ignoring any redefinitions for the output value.
		// These errors aren't fatal since they don't prevent us from returning the value we do have.
		for (path, _) in iter.collect::<Vec<_>>() {
			let prev_path = res.as_ref().unwrap().0;
			errs.push(DbError::RepeatedKey {
				key: r.key.to_string(),
				path: path.to_path_buf(),
				prev_path: prev_path.to_path_buf(),
			});
		}

		res
	}

	pub fn iter_mut<'a: 'e, 'e>(
		&'a mut self,
		errs: &'e mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
	) -> impl Iterator<Item = (&'a Path, KeyMut<'a>, ValidOutputMut<'a, V>)> + 'e {
		let mut paths = HashMap::<String, &PathBuf>::new();

		self.0.iter_mut().flat_map(move |(path, doc)| {
			let tbl = doc.as_table_mut();

			tbl.iter_mut()
				.filter_map(|(key, r)| {
					let r = r.map_err(|e| DbError::InvalidValue {
						path: path.to_path_buf(),
						key: key.to_string(),
						item: e.0.into_owned().into_item(),
					});
					if let Some(it) = r.err_to(errs) {
						if let Some(prev_path) = paths.get(&*key) {
							errs.push(DbError::RepeatedKey {
								key: key.to_string(),
								path: path.to_path_buf(),
								prev_path: (*prev_path).clone(),
							});
						} else {
							paths.insert(key.to_string(), path);
							return Some((path.as_path(), key, it));
						}
					}
					None
				})
				// otherwise this runs into lifetime issues due to the `r` argument...
				.collect::<Vec<_>>()
		})
	}
}

impl MultiDbTree {
	pub fn erased_iter_mut<'a: 'e, 'e>(
		&'a mut self,
		kind: DataKind,
		errs: &'e mut ErrorSink<impl ErrorMonoid<Error = DbError>>,
	) -> Box<dyn Iterator<Item = (&'a Path, KeyMut<'a>, wrapped_toml_edit::ItemMut<'a>)> + 'e> {
		macro_rules! impl_ {
			($e:expr, $V:ident) => {
				Box::new($e.iter_mut(errs).map(|(p, k, v)| (p, k, $V::unvalidate(v))))
			};
		}

		match kind {
			DataKind::Content => impl_!(self.content, IsContent),
			DataKind::ContentType => impl_!(self.types, IsContentType),
			DataKind::Bookmark => impl_!(self.bookmarks, IsBookmark),
			DataKind::Tag => impl_!(self.tags, IsTag),
		}
	}
}
