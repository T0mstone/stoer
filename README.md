# Stör (Stoer)
NOUN: Stör _(m.)_ /ʃtøːr/, [ʃtøːɐ̯], [ʃtœɐ̯] - sturgeon (fish)

(but really, it's a pun on "store")

Stör is a personal database and generalized bookmark manager, based on the [TOML](https://toml.io/en/) format.

## Rationale / Why do you need this?

This tool abstracts a lot of different usecases.
All of them share the same core principle:
> If you want to have a personal database on your computer (i.e. in files you have full control over),
> in a format that's easy to edit manually (i.e. without this program),
> then this is for you.

A few examples:
- The usecase this grew out of originally was a sort of local MyAnimeList,
	so I don't have to rely on a proprietary provider to store this data,
	as well as for privacy reasons
	and also so I can invent my own rating system etc.
	- The bookmark manager part originally also comes from this: To track which episode you're at.
- More generally, any kind of metadata about any media you like can be stored with Stör.
	- For example, you can use it with books you are reading and have read.
		Then, you can use the bookmark manager for literal bookmarks
		(which might be nice if the books you're reading are PDFs on your computer).
- It isn't limited to media either. If you really want, you can use it to [store data about the people you know](https://sive.rs/dbt)
- Another interesting usecase is for users that want to keep website bookmarks separately from their browser.
	(Maybe due to constantly switching between browsers, or maybe out of minimalism)
- This list isn't exhaustive. Stör is intended to be general-purpose software, so there can be many more usecases.
	- If you have a particularly interesting usecase,
		that you think other people might want to know about, please open a PR to amend this list.

## Design
see [Design.md](./Design.md).